package com.ulink.agrostar.pocmvp.login;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Sanket on 4/1/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    @Mock
    LoginContract.View view;

    private LoginPresenter presenter;

    @Before
    public void setup() {
        presenter = new LoginPresenter(view);
    }

    @Test
    public void checkPassingRightArgumentsToValidate_takesToHome() {
        presenter.validateCredentials("Sanket", "Vetkoli");
        verify(view, times(1)).showProgressDialog();
        verify(view, times(1)).goToMainActivity();
        verify(view, times(1)).hideProgressDialog();
    }

    @Test
    public void checkPassingEmptyUsernameToValidate_showsEmptyUserNameWarning() {
        presenter.validateCredentials("", "Vetkoli");
        verify(view, times(1)).showProgressDialog();
        verify(view, times(1)).showUserNameEmptyError();
        verify(view, times(1)).hideProgressDialog();
    }

    @Test
    public void checkPassingEmptyPasswordToValidate_showsEmptyPasswordWarning() {
        presenter.validateCredentials("Sanket", "");
        verify(view, times(1)).showProgressDialog();
        verify(view, times(1)).showPasswordEmptyError();
        verify(view, times(1)).hideProgressDialog();
    }

    @Test
    public void checkOnDestroyOfActivity_viewIsDetachedFromPresenter() {
        presenter.onDestroy();
        assertNull(presenter.getView());
        verify(view, times(1)).hideProgressDialog();
    }

}
