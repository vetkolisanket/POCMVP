package com.ulink.agrostar.pocmvp.login;

/**
 * Created by Sanket on 4/1/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void validateCredentials(String userName, String password) {
        view.showProgressDialog();
        validate(userName, password);
    }

    @Override
    public void onDestroy() {
        view.hideProgressDialog();
        view = null;
    }

    // TODO: 4/1/17 mock handler class
    // TODO: 4/1/17 mock TextUtils class
    // TODO: 4/1/17 make checkForNull and isEmpty functions in Utils class
    private void validate(final String userName, final String password) {
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {*/
                boolean error = false;
                if (userName == null || userName.equals("")) {
                    error = true;
                    view.showUserNameEmptyError();
                }
                if (password == null || password.equals("")) {
                    error = true;
                    view.showPasswordEmptyError();
                }
                if (!error) {
                    view.goToMainActivity();
                }
        view.hideProgressDialog();
           /* }
        }, 2000);*/
    }

    public LoginContract.View getView() {
        return view;
    }

}
