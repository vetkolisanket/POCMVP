package com.ulink.agrostar.pocmvp.login;

/**
 * Created by Sanket on 4/1/17.
 */

public interface LoginContract {

    public interface View {
        void showProgressDialog();

        void hideProgressDialog();

        void showUserNameEmptyError();

        void showPasswordEmptyError();

        void goToMainActivity();

    }

    public interface Presenter {

        void validateCredentials(String userName, String password);

        void onDestroy();

    }

}
