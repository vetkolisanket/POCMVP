package com.ulink.agrostar.pocmvp.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.ulink.agrostar.pocmvp.R;
import com.ulink.agrostar.pocmvp.main.MainActivity;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoginContract.View, View.OnClickListener {

    private AutoCompleteTextView mUserName;
    private EditText mPassword;
    private Button mSignIn;
    private ProgressDialog mProgressDialog;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        attachPresenter();
        init();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    /*****************************CONTRACT START***********************************/
    @Override
    public void showProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void showUserNameEmptyError() {
        mUserName.setError(getString(R.string.error_invalid_email));
    }

    @Override
    public void showPasswordEmptyError() {
        mPassword.setError(getString(R.string.error_invalid_password));
    }

    @Override
    public void goToMainActivity() {
        startActivity(MainActivity.newIntent(this, mUserName.getText().toString().trim()));
    }

    /*****************************CONTRACT END***********************************/

    private void attachPresenter() {
        presenter = new LoginPresenter(this);
    }

    private void init() {
        initViews();
    }

    private void initViews() {
        mUserName = (AutoCompleteTextView) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mSignIn = (Button) findViewById(R.id.email_sign_in_button);
        mSignIn.setOnClickListener(this);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_sign_in_button:
                presenter.validateCredentials(mUserName.getText().toString().trim(),
                        mPassword.getText().toString().trim());
        }
    }



}

