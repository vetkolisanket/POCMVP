package com.ulink.agrostar.pocmvp.login;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ulink.agrostar.pocmvp.R;
import com.ulink.agrostar.pocmvp.main.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Sanket on 5/1/17.
 */
@RunWith(AndroidJUnit4.class)
public class LoginScreenTest {

    @Rule
    public ActivityTestRule<LoginActivity> mLoginActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Rule
    public IntentsTestRule<MainActivity> mMainActivityTestRule = new IntentsTestRule<>(MainActivity.class);

    @Test
    public void testPositiveLoginFlow() {
        String userName = "Sanket";
        String password = "Vetkoli";

        onView(withId(R.id.email)).perform(typeText(userName), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button)).perform(click());
        intended(hasComponent(MainActivity.class.getName()));
    }

    @Test
    public void testFlowWithUserNameEmpty(){
        String password = "Vetkoli";

        onView(withId(R.id.password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button)).perform(click());
        onView(withId(R.id.email)).check(matches(hasErrorText(mLoginActivityTestRule.getActivity()
                .getString(R.string.error_invalid_email))));
    }

    @Test
    public void testFlowWithPasswordEmpty() {
        String userName = "Sanket";

        onView(withId(R.id.email)).perform(typeText(userName), closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button)).perform(click());
        onView(withId(R.id.password)).check(matches(hasErrorText(mLoginActivityTestRule.getActivity()
                .getString(R.string.error_invalid_password))));
    }

}
